package chapter6.service;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.dbunit.Assertion;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.filter.DefaultColumnFilter;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import chapter6.beans.User;
import chapter6.utils.DBUtil;
import junit.framework.TestCase;

public class UserServiceTest  extends TestCase {

	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static final String URL = "jdbc:mysql://localhost/simple_twitter";
	private static final String USER = "root";
	private static final String PASSWORD = "root";

	public UserServiceTest(String name) {
		super(name);
	}

	private File file;

	//DB接続部分(DBUtil)
	static {
		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	private static Connection getConnection() throws Exception {
		Class.forName(DRIVER);
		Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
		return connection;
	}

	@Before
	public void setUp() throws Exception {

		IDatabaseConnection connection = null;
		try {
			super.setUp();
			Connection conn = DBUtil.getConnection();
			connection = new DatabaseConnection(conn);

			//(2)現状のバックアップを取得
			QueryDataSet partialDataSet = new QueryDataSet(connection);
			partialDataSet.addTable("users");
			partialDataSet.addTable("messages");

			file = File.createTempFile("temp", ".xml");
			FlatXmlDataSet.write(partialDataSet,
					new FileOutputStream(file));

			//(3)テストデータを投入する
			IDataSet dataSetBranch = new FlatXmlDataSet(new File("test_data.xml"));
			DatabaseOperation.CLEAN_INSERT.execute(connection, dataSetBranch);

			DBUtil.commit(conn);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
			}
		}
	}

	@After
	public void tearDown() throws Exception {
		IDatabaseConnection connection = null;
		try {
			Connection conn = DBUtil.getConnection();
			connection = new DatabaseConnection(conn);

			IDataSet dataSet = new FlatXmlDataSet(file);
			DatabaseOperation.CLEAN_INSERT.execute(connection, dataSet);

			DBUtil.commit(conn);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			//一時ファイルの削除
			if (file != null) {
				file.delete();
			}
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
			}

		}
	}

	// 以下に課題のテストメソッドを作成
	/**
	 * 参照メソッドのテスト
	 */
	@Test
	public void testInesrtUser() throws Exception {

		List<User> users = new ArrayList<User>();

        User user1 = new UserService().select(1);
		User user2 = new UserService().select(2);

        users.add(user1);
        users.add(user2);

		List<User> resultList = users;

		assertEquals(2, resultList.size());

		User result1 = resultList.get(0);
		assertEquals("account=sample","account=" + result1.getAccount());
		assertEquals("name=sample","name=" + result1.getName());
		assertEquals("email=sample@sample","email=" + result1.getEmail());
		assertEquals("password=aaa111","password=" + result1.getPassword());
		assertEquals("description=yoro","description=" + result1.getDescription());

		User result2 = resultList.get(1);
		assertEquals("account=sample2","account=" + result2.getAccount());
		assertEquals("name=sample2","name=" + result2.getName());
		assertEquals("email=sample2@sample","email=" + result2.getEmail());
		assertEquals("password=aaa222","password=" + result2.getPassword());
		assertEquals("description=shio","description=" + result2.getDescription());
	}

	/**
	 * 更新メソッドのテスト
	 */
	@Test
	public void testUpdataUser() throws Exception {
		//テスト対象となる、storeメソッドを実行
		//テストのインスタンスを生成
		User user1 = new User();
		user1.setId(1);
		user1.setAccount("sample");
		user1.setName("samplesan");
		user1.setEmail("sample1@sample");
		user1.setDescription("yoroshiku");

		User user2 = new User();
		user2.setId(2);
		user2.setAccount("sample3");
		user2.setName("sample2");
		user2.setEmail("sample2@sample");
		user2.setDescription("shiori");

		new UserService().update(user1);
		new UserService().update(user2);

//		テスト結果として期待されるべきテーブルデータを表すITableインスタンスを取得

		IDatabaseConnection connection = null;
		try {
			Connection conn = getConnection();
			connection = new DatabaseConnection(conn);

			//メソッド実行した実際のテーブル
			IDataSet databaseDataSet = connection.createDataSet();
			ITable actualTable = databaseDataSet.getTable("users");

			// テスト結果として期待されるべきテーブルデータを表すITableインスタンスを取得
			IDataSet expectedDataSet = new FlatXmlDataSet(new File("test_data2.xml"));

			ITable expectedTable = expectedDataSet.getTable("users");

		//期待されるITableと実際のITableの比較
			ITable filteredTable = DefaultColumnFilter.includedColumnsTable(actualTable,
					expectedTable.getTableMetaData().getColumns());
			Assertion.assertEquals(expectedTable, filteredTable);
		} finally {
			if (connection != null)
				connection.close();
		}
	}

	@Test
	public void testInsertUser() throws Exception {
		//テスト対象となる、storeメソッドを実行
		//テストのインスタンスを生成
		User user1 = new User();
		user1.setId(3);
		user1.setAccount("sample3");
		user1.setName("sample3");
		user1.setPassword("aaa111");
		user1.setEmail("sample@sample");
		user1.setDescription("yoroshiku");

		User user2 = new User();
		user2.setId(4);
		user2.setAccount("sample4");
		user2.setName("sample4");
		user2.setPassword("aaa222");
		user2.setEmail("sample2@sample");
		user2.setDescription("shiori");

		new UserService().insert(user1);
		new UserService().insert(user2);

//		テスト結果として期待されるべきテーブルデータを表すITableインスタンスを取得

		IDatabaseConnection connection = null;
		try {
			Connection conn = getConnection();
			connection = new DatabaseConnection(conn);

			//メソッド実行した実際のテーブル
			IDataSet databaseDataSet = connection.createDataSet();
			ITable actualTable = databaseDataSet.getTable("users");

			// テスト結果として期待されるべきテーブルデータを表すITableインスタンスを取得
			IDataSet expectedDataSet = new FlatXmlDataSet(new File("insertUser.xml"));

			ITable expectedTable = expectedDataSet.getTable("users");

		//期待されるITableと実際のITableの比較
			ITable filteredTable = DefaultColumnFilter.includedColumnsTable(actualTable,
					expectedTable.getTableMetaData().getColumns());
			Assertion.assertEquals(expectedTable, filteredTable);
		} finally {
			if (connection != null)
				connection.close();
		}
	}

}