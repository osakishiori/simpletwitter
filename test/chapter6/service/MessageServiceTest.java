package chapter6.service;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.dbunit.Assertion;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.filter.DefaultColumnFilter;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import chapter6.beans.Message;
import chapter6.utils.DBUtil;
import junit.framework.TestCase;

public class MessageServiceTest extends TestCase {
	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static final String URL = "jdbc:mysql://localhost/simple_twitter";
	private static final String USER = "root";
	private static final String PASSWORD = "root";

	public MessageServiceTest(String name) {
		super(name);
	}

	private File file;

	//DB接続部分(DBUtil)
	static {
		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	private static Connection getConnection() throws Exception {
		Class.forName(DRIVER);
		Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
		return connection;
	}

	@Before
	public void setUp() throws Exception {
		IDatabaseConnection connection = null;
		try {
			Connection conn = DBUtil.getConnection();
			connection = new DatabaseConnection(conn);

			//(2)現状のバックアップを取得
			QueryDataSet partialDataSet = new QueryDataSet(connection);
			partialDataSet.addTable("users");
			partialDataSet.addTable("messages");

			file = File.createTempFile("temp", ".xml");
			FlatXmlDataSet.write(partialDataSet,
					new FileOutputStream(file));

			//(3)テストデータを投入する
			IDataSet dataSetBranch = new FlatXmlDataSet(new File("test_data.xml"));
			DatabaseOperation.CLEAN_INSERT.execute(connection, dataSetBranch);

			DBUtil.commit(conn);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
			}
		}
	}

	@After
	public void tearDown() throws Exception {
			IDatabaseConnection connection = null;
			try {
				Connection conn = DBUtil.getConnection();
				connection = new DatabaseConnection(conn);

				IDataSet dataSet = new FlatXmlDataSet(file);
				DatabaseOperation.CLEAN_INSERT.execute(connection, dataSet);

				DBUtil.commit(conn);

			} catch (Exception e) {
				e.printStackTrace();
			} finally {

				//一時ファイルの削除
				if (file != null) {
					file.delete();
				}
				try {
					if (connection != null)
						connection.close();
				} catch (SQLException e) {
				}

			}
	}

//	参照メソッド
	public void testSelectMessage() throws Exception {

		List<Message> messages = new ArrayList<Message>();

		Message message1 = new Message();
        message1.setText("sample");
        message1.setId(1);
        message1.setUserId(1);

        Message message2 = new Message();
        message2.setText("sampledayo");
        message2.setId(2);
        message2.setUserId(2);

		//参照メソッドの実行
		 Message msg1 = new MessageService().select(message1.getId());
		 Message msg2 = new MessageService().select(message2.getId());

		 messages.add(msg1);
	     messages.add(msg2);

		 List<Message> resultList = messages;

		//値の検証

		//件数
		assertEquals(2, resultList.size());

		//データ
		Message result001 = resultList.get(0);
		assertEquals("id=1", "id=" + result001.getUserId());
		assertEquals("user_id=1", "user_id=" + result001.getUserId());
		assertEquals("text=sample", "text=" + result001.getText());

		Message result002 = resultList.get(1);
		assertEquals("id=2", "id=" + result002.getUserId());
		assertEquals("user_id=2", "user_id=" + result002.getUserId());
		assertEquals("text=sampledayo", "text=" + result002.getText());
	}

	/**
	 * 更新メソッドのテスト
	 */
	@Test
	public void testInsertMessage() throws Exception {

		Message message1 = new Message();
		message1.setId(3);
        message1.setText("sample");
        message1.setUserId(1);

        Message message2 = new Message();
        message2.setId(4);
        message2.setText("sampledayo");
        message2.setUserId(2);



		new MessageService().insert(message1);
		new MessageService().insert(message2);

//		テスト結果として期待されるべきテーブルデータを表すITableインスタンスを取得

		IDatabaseConnection connection = null;
		try {
			Connection conn = getConnection();
			connection = new DatabaseConnection(conn);

			//メソッド実行した実際のテーブル
			IDataSet databaseDataSet = connection.createDataSet();
			ITable actualTable = databaseDataSet.getTable("messages");

			// テスト結果として期待されるべきテーブルデータを表すITableインスタンスを取得
			IDataSet expectedDataSet = new FlatXmlDataSet(new File("insertMessage.xml"));

			ITable expectedTable = expectedDataSet.getTable("messages");

		//期待されるITableと実際のITableの比較
			ITable filteredTable = DefaultColumnFilter.includedColumnsTable(actualTable,
					expectedTable.getTableMetaData().getColumns());
			Assertion.assertEquals(expectedTable, filteredTable);
		} finally {
			if (connection != null)
				connection.close();
		}
	}


}